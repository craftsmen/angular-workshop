import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { CraftsmenModule } from './craftsmen/craftsmen.module';

const appRoutes: Routes = [
  { path: '',
    redirectTo: '/craftsmen',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CraftsmenModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [
    AppComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
