import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'craftsmen-page',
  templateUrl: './craftsmen-page.component.html'
})
export class CraftsmenPageComponent {
  constructor(private titleService: Title) {}

  private get title() {
    return this.titleService.getTitle();
  }
}
