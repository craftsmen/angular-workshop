import { Component, OnInit } from '@angular/core';
import { Craftsman } from '../craftsman';
import { CraftsmanService } from '../craftsman.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'craftsman-list',
  templateUrl: './craftsman-list.component.html'
})
export class CraftsmanListComponent implements OnInit {
  private craftsmen: Craftsman[];

  constructor(private craftsmanService: CraftsmanService,
              private titleService: Title) {}

  public ngOnInit() {
    this.craftsmanService.craftsmen.subscribe(craftsmen => {
      this.craftsmen = craftsmen;
    });

    this.titleService.setTitle('Overzicht');
  }

  public remove(craftsman: Craftsman): void {
    if (confirm(`Weet je zeker dat je ${craftsman.name} wilt verwijderen?`)) {
      this.craftsmanService.craftsmen.remove(craftsman.$key);
    }
  }
}
