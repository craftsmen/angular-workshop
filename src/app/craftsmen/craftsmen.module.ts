import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CraftsmanListComponent } from './list/craftsman-list.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database/database.module';
import { CraftsmanService } from './craftsman.service';
import { BrowserModule } from '@angular/platform-browser';
import { CraftsmenPageComponent } from './craftsmen-page.component';
import { FormsModule } from '@angular/forms';

const craftsmenRoutes: Routes = [
  {
    path: 'craftsmen',
    component: CraftsmenPageComponent,
    children: [
      {
        path: '',
        component: CraftsmanListComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(craftsmenRoutes),
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp({
      apiKey: 'AIzaSyAJo7OPsgKj6NMuv3wucf2SpoRVlkqT9Mk',
      authDomain: 'localhost',
      databaseURL: 'https://angular-workshop-16151.firebaseio.com/',
      storageBucket: 'gs://angular-workshop-16151.appspot.com/',
      messagingSenderId: '[YOUR NAME HERE]'
    })
  ],
  declarations: [
    CraftsmenPageComponent,
    CraftsmanListComponent
  ],
  providers: [CraftsmanService],
})
export class CraftsmenModule {}
