import { Craftsman } from './craftsman';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database/database';
import { FirebaseListObservable } from 'angularfire2/database/firebase_list_observable';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class CraftsmanService {
  public craftsmen: FirebaseListObservable<Craftsman[]>;

  constructor(private db: AngularFireDatabase) {
    this.craftsmen = this.db.list('/craftsmen');
  }

}
