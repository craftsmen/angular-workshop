export class Craftsman {
  public $key?: string;
  public skills: string[] = [];
  public description: string = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquam commodi ' +
    'consequuntur debitis distinctio doloribus ea esse, harum id itaque labore minus nostrum nulla quae quas quasi ' +
    'rem. Esse, temporibus.';
  public dateAdded: number;

  constructor(
    public name: string,
    public active: boolean,
    public avatar: any
  ) {}
}
