import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Craftsman } from '../craftsman';
import { Router } from '@angular/router';

@Component({
  selector: 'craftsmen-add',
  templateUrl: './craftsman-add.component.html'
})
export class CraftsmanAddComponent implements OnInit {
  private craftsman: Craftsman;

  constructor(private titleService: Title,
              private router: Router) {
  }

  ngOnInit(): void {
    this.titleService.setTitle('Toevoegen');

    this.craftsman = new Craftsman('', false, {
      eyes: 'eyes5',
      nose: 'nose3',
      mouth: 'mouth11',
      color: '#11d898'
    });
  }

  onSubmit() {
    this.craftsman.dateAdded = new Date().getTime();
    // TODO: save the craftsman using CraftsmanService
    this.router.navigate(['./']);
  }
}
